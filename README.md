# .NET 软件工程师（MSD）文档 #
> [达内精品在线 TMOOC.CN](http://www.tmooc.cn "达内精品在线 TMOOC.CN")

----------

## 一、[语言基础 ](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80&oid=fde673c0590c3d9c7023c0891ff56ee397f40dc3&sha=1c625aeb82624cbb404aa43e50b3e533862046c7)##

### 1. [运行环境和开发工具](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/01_%E8%BF%90%E8%A1%8C%E7%8E%AF%E5%A2%83%E5%92%8C%E5%BC%80%E5%8F%91%E5%B7%A5%E5%85%B7?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F01_%E8%BF%90%E8%A1%8C%E7%8E%AF%E5%A2%83%E5%92%8C%E5%BC%80%E5%8F%91%E5%B7%A5%E5%85%B7&oid=1a850aa1cf4406d92e71be299d851ad639b98359&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- .NET Framework （[官方地址](https://www.microsoft.com/net/ ".NET Framework")）
- .NET Core （[官方地址](https://www.microsoft.com/net/core ".NET Core")）
- Visual Studio 2015 Community （[官方地址](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx "Visual Studio 2015 Community")）
- Visual Studio Code （[官方地址](https://code.visualstudio.com "Visual Studio Code")）
- Linq Pad （[官方地址](http://www.linqpad.net/ "Linq Pad")）
- GIT （[官方地址](https://git-scm.com/ "GIT")）
- Internet Information Service（IIS） （[官方地址](http://www.iis.net/ "IIS")）

### 2. [语法和规范](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/02_%E8%AF%AD%E6%B3%95%E5%92%8C%E8%A7%84%E8%8C%83?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F02_%E8%AF%AD%E6%B3%95%E5%92%8C%E8%A7%84%E8%8C%83&oid=4eee21fc9f8ffc9f28ad6a5e1e33b1b9b51d386d&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 文件结构
- 命名规范
- 注释
- 预处理器指令（[MSDN参考](https://msdn.microsoft.com/zh-cn/library/ed8yd1ha.aspx)）
- 分隔符
- 标识符

### 3. [变量和数据类型](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/03_%E5%8F%98%E9%87%8F%E5%92%8C%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F03_%E5%8F%98%E9%87%8F%E5%92%8C%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B&oid=4a5a326f994e26740be6d4967e7020bf1d57f7a4&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 变量
- 常量
- 基本数据类型
- 类型转换
- 装箱拆箱
- 隐式变量
- 动态类型

### 4. [运算符和表达式](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F&oid=8efadc1a400c21d46384503280aab83666255eb0&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
-  赋值运算符
-  算术运算符
-  逻辑运算符
-  关系运算符
-  位运算符

### 5. [控制语句](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/05_%E6%8E%A7%E5%88%B6%E8%AF%AD%E5%8F%A5?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F05_%E6%8E%A7%E5%88%B6%E8%AF%AD%E5%8F%A5&oid=80ba51f6c9c2b02cfc236edfee5a0c2d3566748b&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
-  顺序结构
-  选择结构
-  循环结构
-  循环控制
-  方法
-  参数
-  重载
-  作用域
-  递归

### 06. [异常处理](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/06_%E5%BC%82%E5%B8%B8%E5%A4%84%E7%90%86?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F06_%E5%BC%82%E5%B8%B8%E5%A4%84%E7%90%86&oid=71454a262f5cab6e436b91231c29b47507b45802&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
-  异常分类
-  异常处理
-  断点调试
-  日志处理
-  Log4Net

### 07. [数组与集合](http://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/07_%E6%95%B0%E7%BB%84%E4%B8%8E%E9%9B%86%E5%90%88?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F07_%E6%95%B0%E7%BB%84%E4%B8%8E%E9%9B%86%E5%90%88&oid=b996752f22b54a18c9c9a05925bc73834e74a5d9&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 一维数组
- 多维数组
- 交叉数组
- 数组排序
- 数组应用
- ArrayList
- Hashtable
- Queue
- Stack

## 二、[面向对象（OOP）](http://git.oschina.net/TMOOC/MSD/tree/master/02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1?dir=1&filepath=02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1&oid=a5c8a368522e386b902366a64ab9f23fb0d69806&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##
### 1. [抽象](http://git.oschina.net/TMOOC/MSD/tree/master/02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1/01_%E6%8A%BD%E8%B1%A1?dir=1&filepath=02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1%2F01_%E6%8A%BD%E8%B1%A1&oid=148aad8f25baccc8a21f80835125f6e720b62708&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 类型声明
- 类成员
- 实例成员
- 静态成员
- 内存管理

### 2. [封装](http://git.oschina.net/TMOOC/MSD/tree/master/02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1/02_%E5%B0%81%E8%A3%85?dir=1&filepath=02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1%2F02_%E5%B0%81%E8%A3%85&oid=7f84098911fe965b67e5dcb53b7b69fae848c918&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 访问修饰符
- 属性
- 内部类
- 匿名类型

### 3 [继承](http://git.oschina.net/TMOOC/MSD/tree/master/02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1/03_%E7%BB%A7%E6%89%BF?dir=1&filepath=02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1%2F03_%E7%BB%A7%E6%89%BF&oid=1311a71ebfafdb28de24c533d27d52062d3754ef&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 代码复用
- 方法重写
- 方法隐藏
- 抽象类
- 抽象方法
- 密封类

### 4. [多态](http://git.oschina.net/TMOOC/MSD/tree/master/02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1/04_%E5%A4%9A%E6%80%81?dir=1&filepath=02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1%2F04_%E5%A4%9A%E6%80%81&oid=e97b7533e43e99a97698d6e2217b3259d74f4374&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 接口
- 类型检查
- 里氏替换原则(LSP)

### 5. [泛型](http://git.oschina.net/TMOOC/MSD/tree/master/02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1/05_%E6%B3%9B%E5%9E%8B?dir=1&filepath=02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1%2F05_%E6%B3%9B%E5%9E%8B&oid=ebdff69af2b5acd580a2bcaebd02bbb58e0a5080&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
-  泛型类
-  泛型接口
-  泛型方法
-  泛型约束

### 6. [设计模式](http://git.oschina.net/TMOOC/MSD/tree/master/02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1/06_%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F?dir=1&filepath=02_%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1%2F06_%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F&oid=f71a18452512831945bc54461f6fa1004c4b8394&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- 设计原则（Principle）
- 创建型（Creational）
- 结构型（Structural）
- 行为型（Behavioral）
- 重构（Refactoring）

## 三、[框架集（Framework）](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86&oid=a6c5efb6ae6ffb8ee88bfef44d18704a700df1c2&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##

### 1. [反射](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86/01_%E5%8F%8D%E5%B0%84?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86%2F01_%E5%8F%8D%E5%B0%84&oid=d26403ab1445b14a3d9e4ca7b46061042f7db90c&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 2. [输入输出](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86/02_%E8%BE%93%E5%85%A5%E8%BE%93%E5%87%BA?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86%2F02_%E8%BE%93%E5%85%A5%E8%BE%93%E5%87%BA&oid=9482f0da2f84f6460bc206e4ffb67abccc5ef1de&sha=dea95cba6bf7674a662b806d2975d739ee246b62)(I/O) ###
### 3. [线程](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86/03_%E7%BA%BF%E7%A8%8B?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86%2F03_%E7%BA%BF%E7%A8%8B&oid=993d44778ef0468ebf1c499dd58d7a82c30e0726&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 4. [通信](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86/04_%E9%80%9A%E4%BF%A1?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86%2F04_%E9%80%9A%E4%BF%A1&oid=aca8c0cb483bea36a8b103d74ad5f933d8e7b964&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 5. [委托](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86/05_%E5%A7%94%E6%89%98?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86%2F05_%E5%A7%94%E6%89%98&oid=55ff6e0c6e2cc1eb75a65097e4cd8333b5735ba4&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 6. [字符串](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86/06_%E5%AD%97%E7%AC%A6%E4%B8%B2?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86%2F06_%E5%AD%97%E7%AC%A6%E4%B8%B2&oid=05f8e1308caa05085ed1f8752b3184125da86625&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 7. [图形处理](http://git.oschina.net/TMOOC/MSD/tree/master/03_%E6%A1%86%E6%9E%B6%E9%9B%86/07_%E5%9B%BE%E5%BD%A2%E5%A4%84%E7%90%86?dir=1&filepath=03_%E6%A1%86%E6%9E%B6%E9%9B%86%2F07_%E5%9B%BE%E5%BD%A2%E5%A4%84%E7%90%86&oid=bcae6021e64574c1592e8004fc9208f0601b5504&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###

## 四、[数据管理（DBMS）](http://git.oschina.net/TMOOC/MSD/tree/master/04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86?dir=1&filepath=04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86&oid=5e20a9cb43b568fcf8debae47c0dd334ec3d238f&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##

### 1. [XML处理](http://git.oschina.net/TMOOC/MSD/tree/master/04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86/01_XML%E5%A4%84%E7%90%86?dir=1&filepath=04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86%2F01_XML%E5%A4%84%E7%90%86&oid=81e81ce92eb8acd40de8eb56907f2974f6e8bc05&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 2. [JSON处理](http://git.oschina.net/TMOOC/MSD/tree/master/04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86/02_JSON%E5%A4%84%E7%90%86?dir=1&filepath=04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86%2F02_JSON%E5%A4%84%E7%90%86&oid=c0700d42b7ee2169954de0db009008595bf03044&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 3.[SQLSERVER](http://git.oschina.net/TMOOC/MSD/tree/master/04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86/03_SQLSERVER?dir=1&filepath=04_%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86%2F03_SQLSERVER&oid=deace948cf993bdddf71c2ed7f1c4be5bc605454&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###

## 五、[数据处理](http://git.oschina.net/TMOOC/MSD/tree/master/05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86?dir=1&filepath=05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86&oid=ef009c5cdcfdec5b39ad26817fc2a73a7102f0a1&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##
### 1. [ADO.NET](http://git.oschina.net/TMOOC/MSD/tree/master/05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86/01_ADO.NET?dir=1&filepath=05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86%2F01_ADO.NET&oid=c726fcb772a288fe585d2c4bf9e0f2d4bee09a84&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
- Connection
- Command
- DataAdapter
- DataSet
- DataReader
- SqlHelper

### 2. [LINQ](http://git.oschina.net/TMOOC/MSD/tree/master/05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86/02_LINQ?dir=1&filepath=05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86%2F02_LINQ&oid=af8dcc0450cc89c2dce00180de89af630963bdbe&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 3. [EF框架（Entity Framework）](http://git.oschina.net/TMOOC/MSD/tree/master/05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86/03_EF%E6%A1%86%E6%9E%B6(Entity%20Framework)?dir=1&filepath=05_%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86%2F03_EF%E6%A1%86%E6%9E%B6%28Entity+Framework%29&oid=c2a8006bd3ad3c6e073a1c514cde5e0f48aae65a&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###

## 六、[窗口应用](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8&oid=018aa20852d1a54951196e38bb051794b680d6af&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##

### 1. [事件驱动](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/01_%E4%BA%8B%E4%BB%B6%E9%A9%B1%E5%8A%A8?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F01_%E4%BA%8B%E4%BB%B6%E9%A9%B1%E5%8A%A8&oid=7f0ac670d1ab25ef877ce7e3fe5d9c02d0f905d3&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 2. [窗体布局](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/02_%E7%AA%97%E4%BD%93%E5%B8%83%E5%B1%80?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F02_%E7%AA%97%E4%BD%93%E5%B8%83%E5%B1%80&oid=43a5cc75a4ce9efecc6514c32aa5c82bcbf231e2&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 3. [公共控件](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/03_%E5%85%AC%E5%85%B1%E6%8E%A7%E4%BB%B6?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F03_%E5%85%AC%E5%85%B1%E6%8E%A7%E4%BB%B6&oid=bf4684460cf2bab856a14e665fbde01f7e9fabb4&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 4. [菜单控件](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/04_%E8%8F%9C%E5%8D%95%E6%8E%A7%E4%BB%B6?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F04_%E8%8F%9C%E5%8D%95%E6%8E%A7%E4%BB%B6&oid=0a5aa1c7b0db8e7131ff24edbf859511ef53e58f&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 5. [容器控件](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/05_%E5%AE%B9%E5%99%A8%E6%8E%A7%E4%BB%B6?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F05_%E5%AE%B9%E5%99%A8%E6%8E%A7%E4%BB%B6&oid=d7476882c396a054c9da76a18a5fb30afe1e8b4a&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 6. [对话框控件](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/06_%E5%AF%B9%E8%AF%9D%E6%A1%86%E6%8E%A7%E4%BB%B6?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F06_%E5%AF%B9%E8%AF%9D%E6%A1%86%E6%8E%A7%E4%BB%B6&oid=b81972a6ab118369c097938e49cd93ac46f09289&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 7. [打印控件](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/07_%E6%89%93%E5%8D%B0%E6%8E%A7%E4%BB%B6?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F07_%E6%89%93%E5%8D%B0%E6%8E%A7%E4%BB%B6&oid=ac467b1f242e145faeaa59c6f8a6b43e2f131753&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 8. [数据绑定控件](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/08_%E6%95%B0%E6%8D%AE%E7%BB%91%E5%AE%9A%E6%8E%A7%E4%BB%B6?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F08_%E6%95%B0%E6%8D%AE%E7%BB%91%E5%AE%9A%E6%8E%A7%E4%BB%B6&oid=8667e62dfbc12d4ff566c0d86877e1f7e997a61d&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 9. [统计报表](http://git.oschina.net/TMOOC/MSD/tree/master/06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8/09_%E7%BB%9F%E8%AE%A1%E6%8A%A5%E8%A1%A8?dir=1&filepath=06_%E7%AA%97%E5%8F%A3%E5%BA%94%E7%94%A8%2F09_%E7%BB%9F%E8%AE%A1%E6%8A%A5%E8%A1%A8&oid=1122f2b91b09b7ded3a939ba37a03a89b302793d&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###

## 七、[Web前端](http://git.oschina.net/TMOOC/MSD/tree/master/07_Web%E5%89%8D%E7%AB%AF?dir=1&filepath=07_Web%E5%89%8D%E7%AB%AF&oid=a9d854fd74a75151cc9e5ff4fa42493b669443db&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##
###  1. [HTML](http://git.oschina.net/TMOOC/MSD/tree/master/07_Web%E5%89%8D%E7%AB%AF/01_HTML?dir=1&filepath=07_Web%E5%89%8D%E7%AB%AF%2F01_HTML&oid=01cb3f47fc250e4a549edc72b0b3cce13db42167&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
###  2. [CSS](http://git.oschina.net/TMOOC/MSD/tree/master/07_Web%E5%89%8D%E7%AB%AF/02_CSS?dir=1&filepath=07_Web%E5%89%8D%E7%AB%AF%2F02_CSS&oid=e6cad35bb3c5bb7fedca184b67b88a7b8037110f&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
###  3. [JavaScript](http://git.oschina.net/TMOOC/MSD/tree/master/07_Web%E5%89%8D%E7%AB%AF/03_JavaScript?dir=1&filepath=07_Web%E5%89%8D%E7%AB%AF%2F03_JavaScript&oid=772aec96835d507b6427dcf1831045b66c94d8c1&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
###  4. [jQuery](http://git.oschina.net/TMOOC/MSD/tree/master/07_Web%E5%89%8D%E7%AB%AF/04_jQuery?dir=1&filepath=07_Web%E5%89%8D%E7%AB%AF%2F04_jQuery&oid=5cd0102f1afd1b2c2282721e63373e311ea376bb&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
###  5. [Bootstrap](http://git.oschina.net/TMOOC/MSD/tree/master/07_Web%E5%89%8D%E7%AB%AF/05_Bootstrap?dir=1&filepath=07_Web%E5%89%8D%E7%AB%AF%2F05_Bootstrap&oid=ac31a2e929c228e6e2fa1d72d4255f13522025c6&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###

## 八、[ASP.NET](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET?dir=1&filepath=08_ASP.NET&oid=bf8b1d1e716a8f5b2cb8b059f89180190af41a48&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##
### 1. [B/S架构](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/01_BS%E6%9E%B6%E6%9E%84?dir=1&filepath=08_ASP.NET%2F01_BS%E6%9E%B6%E6%9E%84&oid=f7952909420da2bedc2d15e080101e2478a55797&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 2. [运行机制](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/02_%E8%BF%90%E8%A1%8C%E6%9C%BA%E5%88%B6?dir=1&filepath=08_ASP.NET%2F02_%E8%BF%90%E8%A1%8C%E6%9C%BA%E5%88%B6&oid=08a2b3b9cf6978b51213b03e83808ca2fc79fe51&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 3. [HttpHandler](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/03_HttpHandler?dir=1&filepath=08_ASP.NET%2F03_HttpHandler&oid=cabdb283e2c509d92a639b4b0c029c1e16d34dae&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 4. [HttpModule](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/04_HttpModule?dir=1&filepath=08_ASP.NET%2F04_HttpModule&oid=e2b55ecb788fa6d82e8007ff87e5936b85f2c11c&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 5. [内置对象](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/05_%E5%86%85%E7%BD%AE%E5%AF%B9%E8%B1%A1?dir=1&filepath=08_ASP.NET%2F05_%E5%86%85%E7%BD%AE%E5%AF%B9%E8%B1%A1&oid=93e9d33a1d389aab08d42b14e0329af5ce2c24ca&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 6. [状态保持](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/06_%E7%8A%B6%E6%80%81%E4%BF%9D%E6%8C%81?dir=1&filepath=08_ASP.NET%2F06_%E7%8A%B6%E6%80%81%E4%BF%9D%E6%8C%81&oid=4cf89a3bc5218702b8fbec558d38fe8ac2671050&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 7. [页面跳转](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/07_%E9%A1%B5%E9%9D%A2%E8%B7%B3%E8%BD%AC?dir=1&filepath=08_ASP.NET%2F07_%E9%A1%B5%E9%9D%A2%E8%B7%B3%E8%BD%AC&oid=0cd84f4af132dca65a674bb8eec7e3e5e12e91ce&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 8. [布局和导航](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/08_%E5%B8%83%E5%B1%80%E5%92%8C%E5%AF%BC%E8%88%AA?dir=1&filepath=08_ASP.NET%2F08_%E5%B8%83%E5%B1%80%E5%92%8C%E5%AF%BC%E8%88%AA&oid=6c340ce6078d08dfc2cf6788a6eee651f084eb56&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 9. [数据绑定](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/09_%E6%95%B0%E6%8D%AE%E7%BB%91%E5%AE%9A?dir=1&filepath=08_ASP.NET%2F09_%E6%95%B0%E6%8D%AE%E7%BB%91%E5%AE%9A&oid=35f29861329e0d309634280d214e3974d2de333f&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 10. [安全框架](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/10_%E5%AE%89%E5%85%A8%E6%A1%86%E6%9E%B6?dir=1&filepath=08_ASP.NET%2F10_%E5%AE%89%E5%85%A8%E6%A1%86%E6%9E%B6&oid=2e248894e118b1e43bbcfcd4603ec088d3d2cb5b&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 11. [缓存依赖](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/11_%E7%BC%93%E5%AD%98%E4%BE%9D%E8%B5%96?dir=1&filepath=08_ASP.NET%2F11_%E7%BC%93%E5%AD%98%E4%BE%9D%E8%B5%96&oid=d7c2632e411eb48946bbde7817172ab051135e16&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 12. [AJAX](http://git.oschina.net/TMOOC/MSD/tree/master/08_ASP.NET/12_AJAX?dir=1&filepath=08_ASP.NET%2F12_AJAX&oid=2ff01aeaf9c1f4b241f5a5442df37e70ea377590&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###

## 九、[ASP.NET MVC](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC?dir=1&filepath=09_ASP.NET+MVC&oid=3e293e8ce9425ca609e40d053880f24c524b8d16&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##
### 1. [MVC模式](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/01_MVC%E6%A8%A1%E5%BC%8F?dir=1&filepath=09_ASP.NET+MVC%2F01_MVC%E6%A8%A1%E5%BC%8F&oid=0a3cc3d8925db3b20edb6cc690ebbaa1327afb85&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 2. [路由](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/02_%E8%B7%AF%E7%94%B1?dir=1&filepath=09_ASP.NET+MVC%2F02_%E8%B7%AF%E7%94%B1&oid=af4529416275de0addc9f6fb8a5623c9db7b9afa&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 3. [控制器](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/03_%E6%8E%A7%E5%88%B6%E5%99%A8?dir=1&filepath=09_ASP.NET+MVC%2F03_%E6%8E%A7%E5%88%B6%E5%99%A8&oid=6af4962827dbf1bbd6be13e135de37b328a68fa1&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 4. [视图](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/04_%E8%A7%86%E5%9B%BE?dir=1&filepath=09_ASP.NET+MVC%2F04_%E8%A7%86%E5%9B%BE&oid=a658dddedc7119efb70fd4654fa863e9c96282d7&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 5. [Razor](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/05_Razor?dir=1&filepath=09_ASP.NET+MVC%2F05_Razor&oid=7bc6a0f5046246ea2302cb82e07c44958023662d&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 6. [模板](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/06_%E6%A8%A1%E6%9D%BF?dir=1&filepath=09_ASP.NET+MVC%2F06_%E6%A8%A1%E6%9D%BF&oid=515a618b88ca782a03688edfae8b27466e39ff77&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 7. [辅助方法](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/07_%E8%BE%85%E5%8A%A9%E6%96%B9%E6%B3%95?dir=1&filepath=09_ASP.NET+MVC%2F07_%E8%BE%85%E5%8A%A9%E6%96%B9%E6%B3%95&oid=ccfec669942f8038f6e35369cfcb37f5670736a8&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 8. [扩展方法](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/08_%E6%89%A9%E5%B1%95%E6%96%B9%E6%B3%95?dir=1&filepath=09_ASP.NET+MVC%2F08_%E6%89%A9%E5%B1%95%E6%96%B9%E6%B3%95&oid=5c395efadb684ab052c9c65771f488eb9a07fa83&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 9. [模型](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/09_%E6%A8%A1%E5%9E%8B?dir=1&filepath=09_ASP.NET+MVC%2F09_%E6%A8%A1%E5%9E%8B&oid=bda4d51be854d4bbd0310294dd1b902675dddc3e&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 10. [实体框架集](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/10_%E5%AE%9E%E4%BD%93%E6%A1%86%E6%9E%B6%E9%9B%86?dir=1&filepath=09_ASP.NET+MVC%2F10_%E5%AE%9E%E4%BD%93%E6%A1%86%E6%9E%B6%E9%9B%86&oid=8da9f7f4a3ab9f43b65494b2b8699945c3a55ab1&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 11. [注解](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/11_%E6%B3%A8%E8%A7%A3?dir=1&filepath=09_ASP.NET+MVC%2F11_%E6%B3%A8%E8%A7%A3&oid=ebcca480815623fe66f18cdf6e4139feca26cc54&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 12. [过滤器](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/12_%E8%BF%87%E6%BB%A4%E5%99%A8?dir=1&filepath=09_ASP.NET+MVC%2F12_%E8%BF%87%E6%BB%A4%E5%99%A8&oid=fe005cb3c74076696a491ed427c2f34f6e22665f&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 13. [安全](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/13_%E5%AE%89%E5%85%A8?dir=1&filepath=09_ASP.NET+MVC%2F13_%E5%AE%89%E5%85%A8&oid=e23ddecc24158cf8c12973100054baff2fef84d6&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 14. [性能](http://git.oschina.net/TMOOC/MSD/tree/master/09_ASP.NET%20MVC/14_%E6%80%A7%E8%83%BD?dir=1&filepath=09_ASP.NET+MVC%2F14_%E6%80%A7%E8%83%BD&oid=b6e4c0e2e334482c2e96d0b973f8c0502ed51835&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###

## 十、[跨平台](http://git.oschina.net/TMOOC/MSD/tree/master/10_%E8%B7%A8%E5%B9%B3%E5%8F%B0?dir=1&filepath=10_%E8%B7%A8%E5%B9%B3%E5%8F%B0&oid=c9ef1b499cc9b5f4ed77001b1cd267f2aa38ce19&sha=1c625aeb82624cbb404aa43e50b3e533862046c7) ##
### 1. [.NET Core](http://git.oschina.net/TMOOC/MSD/tree/master/10_%E8%B7%A8%E5%B9%B3%E5%8F%B0/01_.NET%20Core?dir=1&filepath=10_%E8%B7%A8%E5%B9%B3%E5%8F%B0%2F01_.NET+Core&oid=35fc94245ae91826d2c97a39559a0813926b8ed0&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###
### 2. [Xamarin](http://git.oschina.net/TMOOC/MSD/tree/master/10_%E8%B7%A8%E5%B9%B3%E5%8F%B0/02_Xamarin?dir=1&filepath=10_%E8%B7%A8%E5%B9%B3%E5%8F%B0%2F02_Xamarin&oid=c7c82e35603b16b8413448e9097882aa51e8768a&sha=dea95cba6bf7674a662b806d2975d739ee246b62) ###