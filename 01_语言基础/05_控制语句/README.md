# .NET 软件工程师（MSD）文档 #
> [达内精品在线 TMOOC.CN](http://www.tmooc.cn "达内精品在线 TMOOC.CN")  
> [MSD](https://git.oschina.net/TMOOC/MSD) > [语言基础](https://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80) > [控制语句](https://git.oschina.net/TMOOC/MSD/tree/master/01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80/05_%E6%8E%A7%E5%88%B6%E8%AF%AD%E5%8F%A5?dir=1&filepath=01_%E8%AF%AD%E8%A8%80%E5%9F%BA%E7%A1%80%2F05_%E6%8E%A7%E5%88%B6%E8%AF%AD%E5%8F%A5&oid=80ba51f6c9c2b02cfc236edfee5a0c2d3566748b&sha=ad0867223066b54ed27eaa1f99a12835bab5b78b)

----------

控制语句来自结构化程序设计（POA，POD，POP）  
- POA：Procedure Oriented Analysis
- POD：Procedure Oriented Design
- POP：Procedure Oriented Programming

## 顺序结构 ##
有序的计算机指令集合，自上而下，依次执行  

```
void Main()
{
	Console.WriteLine ("这是第 01 行代码");
	Console.WriteLine ("这是第 02 行代码");
	Console.WriteLine ("这是第 03 行代码");
}
```

## 选择结构 ##
根据判断的结果（true | false）选择执行流程  
主要的语法结构有： if 、if-else、if-else if-else、三目（三元）运算符、switch

### if 语句 ###
```C#
void Main()
{
	int score = 60;
	if (score>=60)
	{
		Console.WriteLine ("考试及格了");
	}
}
```

### if-else 语句 ###
```C#
void Main()
{
	int score = 55;
	if (score >= 60)
	{
		Console.WriteLine ("考试及格了");
	}
	else{
		Console.WriteLine ("考试不及格");
	}
}
```

### if-else if-else 语句 ###
```
void Main()
{
	int score = 50;
	if (score<60)
	{
		Console.WriteLine ("考试不及格");
	}else if(score>=60 && score<85){
		Console.WriteLine ("考试及格了");
	}else{
		Console.WriteLine ("考试非常优秀");
	}
}
```
注意：逻辑错误有可能会导致某个选择分支永远无法被执行，比如
```
void Main()
{
	int score = 90;
	if (score>=60)
	{
		Console.WriteLine ("考试及格了");
	}else if(score>=85){
		Console.WriteLine ("考试非常优秀");		// 考试成绩大于85分的分支永远无法进入
	}else{
		Console.WriteLine ("考试不及格");
	}
}
```
提示：选择出现概率大的分支放置在最上面，可以提高代码运行的效率，比如
```
void Main()
{
	int score = 90;
	if (score>=60 && score<85)
	{
		Console.WriteLine ("考试及格了");		// 一个班级中考试及格的学员应该是比例最高的
	}else if(score>=85){
		Console.WriteLine ("考试非常优秀");
	}else{
		Console.WriteLine ("考试不及格");
	}
}
```

### 三目（三元）运算符 ###
根据条件选择执行的代码块
```
void Main()
{
	int score = 60;
	Console.WriteLine (score>=60 ? "及格了" : "不及格");
}
```

### switch 语句 ###
根据给定的条件选择对应的分支（可以贯穿多个）执行，比如
```
void Main()
{
	switch (DateTime.Now.DayOfWeek)
	{
		case DayOfWeek.Sunday:
		case DayOfWeek.Saturday:
			Console.WriteLine ("周六日休息");
			break;	// 很重要，中断switch语句块
		case DayOfWeek.Monday:
		case DayOfWeek.Tuesday:
		case DayOfWeek.Wednesday:
		case DayOfWeek.Thursday:
		case DayOfWeek.Friday:
			Console.WriteLine ("周一至周五工作日");
			break;
		default:	// 其它默认情况
			Console.WriteLine ("你在逗我吗？");
			break;
	}
}
```

## 循环结构 ##
根据条件判断是否重复执行指定的代码  
主要的语法结构有：while、do-while、for、foreach（迭代）  
循环三要素：循环变量、循环体、循环终止条件

### while 语句 ###
先判断条件，如果成立则进行循环
```
void Main()
{
	int n = 1;
	while (n<=5)
	{
		Console.WriteLine ("第{0}次循环, 此时n={0}", n);
		n+=1;	// 很重要，如果n不增加会变成死循环
	}
}
```

### do-while 语句 ###
先执行循环语句，然后再判断
```
void Main()
{
	int n = 6;
	do
	{
		// 即使不符合循环条件，也会先执行一次
		Console.WriteLine ("执行循环语句，此时n={0}", n);
	} while (n<=5);
}
```

### for 语句 ###
非常棒的循环控制语句，体现了循环三要素：循环变量、循环条件、循环语句
```
void Main()
{
	/*
	*	i=1  是初始值
	*	i<=5 是判断条件
	*	i++  是步进长度
	*/
	for (int i = 1; i <= 5; i++)
	{
		Console.WriteLine ("执行循环代码，此时i={0}", i);
	}
}
```
某些情况下，for语句中的三部分表达式都可以写到其它位置，但不常用，比如
```
void Main()
{
	int i = 1;
	for (;;)
	{		
		if(i<=5){
			Console.WriteLine ("执行循环代码，此时i={0}", i);
		}else{
			break;
		}
		i++;
	}
}
```

### 循环嵌套 ###
分支、循环语句都可以相互嵌套，比如
```
void Main()
{
	for (int i = 0; i < 10; i++)
	{
		if (i%2==0)
		{
			Console.WriteLine ("{0}是偶数", i);
		}
	}
}
```

也可以通过嵌套实现九九乘法表
```
void Main()
{
	for (int i = 1; i <= 9; i++)
	{
		for (int j = 1; j <= 9; j++)
		{
			Console.Write ("{0}*{1}={2}\t", i, j, (i*j).ToString().PadLeft(2,'0'));
		}
		Console.WriteLine ();
	}
}
```

## 循环控制 ##
控制在循环过程中代码的走向 
主要的语法结构有：break、continue、return

### break 语句 ###
break可以中断循环  
在循环嵌套中，只能中断break所在的循环，比如
```
void Main()
{
	for (int i = 1; i <= 10; i++)
	{
		Console.WriteLine ("开始第{0}轮循环", i);
		for (int j = 1; j <= 10; j++)
		{
			if (i==j)
			{
				Console.WriteLine ("i和j相等，中断内部循环");
				break;
			}			
			Console.WriteLine ("i={0}, j={1}", i, j);
		}
	}
}
```

### continue 语句 ###
continue可以跳出本次循环，继续下一次循环  
同样continue也是只可以作用于它所在的循环体，比如
```
void Main()
{
	for (int i = 1; i <= 10; i++)
	{
		Console.WriteLine ("开始第{0}轮循环", i);
		for (int j = 1; j <= 10; j++)
		{			
			if (i==j)
			{
				Console.WriteLine ("i和j相等，跳出本次循环");
				continue;				
			}
			Console.WriteLine ("i={0}, j={1}", i, j);
		}
	}
}
```

### return 语句 ###
return直接退出方法，并返回指定的结果
```
void Main()
{
	for (int i = 1; i <= 10; i++)
	{
		Console.WriteLine ("开始第{0}轮循环", i);
		for (int j = 1; j <= 10; j++)
		{
			if (i==3 && j==3)
			{
				Console.WriteLine ("i和j都为3的时候，退出方法");
				return;
			}			
			Console.WriteLine ("i={0}, j={1}", i, j);
		}
	}
}
```

## [方法](https://msdn.microsoft.com/zh-cn/library/ms173114.aspx) ##
方法是包含一系列语句的代码块，面向过程编程中最小的逻辑单元，实现代码复用  
主要的知识点有：声明方法、方法调用  
要点：关注方法的 返回类型 和 参数列表  
```
访问修饰符 返回类型 方法名(参数类型 参数名)
{
	// 方法的实现代码
}
```
例如：计算两个数字相加的方法
```
// 调用方法
void Main()
{
	Print();
	Console.WriteLine (Add(12,3));
}

// 声明方法（没有参数和返回类型）
public void Print(){
	Console.WriteLine ("实现两个数字相加");
}
// 声明方法（有参数和返回类型）
public double Add(double numA, double numB){
	double sum = numA + numB;
	return sum;
}
```

方法允许继续调用方法，从而实现代码的复用，比如
```
// 主函数
void Main()
{
	Print(FormatDate(GetNow()));
}

// 获取系统时间
public DateTime GetNow()
{
	return DateTime.Now;
}

// 格式化时间
public string FormatDate(DateTime dt)
{
	return string.Format("{0:yyyy-MM-dd}", dt);
}

// 输出字符串
public void Print(string str)
{
	Console.WriteLine (str);
}
```

方法也可以自己调用自己
```
void Main()
{
	double result = Add(Add(Add(1,2),3),4);
	Console.WriteLine (result);	//自己运行一下，看看结果和想的是否一致
}
// 计算两个数字相加的和
public double Add(double numA, double numB) {
	return numA + numB;
}
```

## 作用域 ##
变量所在的范围决定了变量的生命周期和优先级  
通常分为：全局变量 和 局部变量  
全局变量允许被类中多个方法访问，局部变量只能在方法内有效
```
void Main()
{
	Scope s = new Scope();
	s.Show();
	s.Print();
}

class Scope
{
	public string Name = "全局变量";
	
	public void Show(){
		string name = "Show方法中的局部变量";
		Console.WriteLine (Name);
		Console.WriteLine (name);
	}
	
	public void Print(){
		string name = "Print方法中的局部变量";
		Console.WriteLine (Name);
		Console.WriteLine (name);
	}
}
```

## [参数](https://msdn.microsoft.com/zh-cn/library/8f1hz171.aspx) ##
方法运行过程中需要提供的外部数据（值），提高了方法的灵活性（弹性）  
参数的主要分类有：值参数、引用参数、输出参数、参数数组、命名（关键字）参数、可选（默认值）参数

### 值参数 ###
值参数无法影响方法外部的变量值，如下面的代码，执行完代码后，变量m的值不会发生变化
```
void Main()
{
	int m = 1;
	AddSelf(m);
	Console.WriteLine (m);		//虽然在方法中加1，但此时m依然是1
}

public void AddSelf(int num){
	num++;
	Console.WriteLine (num);
}
```

在参数类型是引用类型的时候（string除外），是可以影响外部变量的值，比如
```
void Main()
{
	Book b = new Book{ Title=".NET" };
	ChangeTitle(b);
	Console.WriteLine (b.Title);		//此时图书标题是 'C#'
}

// 修改图书标题的方法
void ChangeTitle(Book book){
	book.Title = "C#";
}

// 用户自定义的类是引用类型
class Book
{
	public string Title;
}
```

### 引用参数 ref ###
引用参数可以修改方法外部变量的值，比如
```
void Main()
{
	int i = 1;
	SelfAdd(ref i);		// 调用方法的时候记得添加ref关键字
	Console.WriteLine (i);
}

public void SelfAdd(ref int num) {
	num++;
}
```

### 输出参数 out ###
某些情况下，输出参数和引用参数的表现一直，也可以影响方法外部变量的值  
输出参数通常用于实现一个方法返回多个类型的值（这很有用）
```
void Main()
{
	int s;
	int b;
	int m;
	int d;
	Console.WriteLine (Calc(12,3,out s,out b,out m, out d));
	Console.WriteLine ("{0},{1},{2},{3}",s,b,m,d);
}

public string Calc(int a, int b, out int sum, out int sub, out int mul, out int div) {	
	sum = a+b;
	sub = a-b;
	mul = a*b;
	div = a/b;
	return "执行结果是:";
}
```

## 重载 ##
同一个类中，方法名相同，参数列表不同  
运行过程中根据方法签名（方法名+参数列表）自动选择最匹配的方法执行  
注意：重载（overload）与方法的返回类型无关  
提示：新的C#语法中，推出了[匿名方法](https://msdn.microsoft.com/zh-cn/library/0yw3tz5k.aspx)和[Lambda表达式](https://msdn.microsoft.com/zh-cn/library/bb397687.aspx)


## 递归 ##
递归是一种算法思想：方法一直调用自己直到满足条件时终止  
递归需要注意2点：效率 和 终止条件（否则容易形成死循环）